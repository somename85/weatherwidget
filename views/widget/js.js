/**
 * Created by blackbookman on 25.03.15.
 */
!function() {
    window.uglyWeatherWidget = window.uglyWeatherWidget || {};
    uglyWeatherWidget.CallbackRegistry = {};

    var cont = document.getElementById('ugly-weather-widget'),
        timePreviousUpdate = null;

    updateWidget();
    setInterval(updateWidget, 1000 * 60);

    function updateWidget() {
        if (!checkTime()) return;

        getWeatherData(uglyWeatherWidget.daysNum, uglyWeatherWidget.cityId, function(result) {
            cont.innerHTML = uglyWeatherWidget.build({array: result});
            timePreviousUpdate = new Date();
        }, function(err) {
            console.log(err);
            cont.innerHTML = err.message || 'Widget error. Reload page or wait sometime.';
        });
    }
    function checkTime() {
        return new Date() - timePreviousUpdate >= 1000 * 60 * 3;
    }
    function scriptRequest(url, onSuccess, onError) {
        var scriptOk = false;
        var callbackName = 'f'+String(Math.random()).slice(2);

        url += ~url.indexOf('?') ? '&' : '?';
        url += 'callback=uglyWeatherWidget.CallbackRegistry.'+callbackName;

        uglyWeatherWidget.CallbackRegistry[callbackName] = function(data) {
            scriptOk = true;
            delete uglyWeatherWidget.CallbackRegistry[callbackName];
            onSuccess(data);
        };

        function checkCallback() {
            if (scriptOk) return;
            delete uglyWeatherWidget.CallbackRegistry[callbackName];
            onError(url);
        }

        var script = document.createElement('script');

        script.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded'){
                this.onreadystatechange = null;
                setTimeout(checkCallback, 0);
            }
        };

        script.onload = script.onerror = checkCallback;
        script.src = url;

        document.body.appendChild(script);
    }
    function getWeatherData(days, cityId, onSuccess, onError) {
        if (typeof days !== "number" || typeof cityId !== 'number') return;

        var url = 'http://localhost:3000/weather';

        scriptRequest(url + '?daysNum=' + days + '&cityId=' + cityId, onSuccess, onError);
    }
}();