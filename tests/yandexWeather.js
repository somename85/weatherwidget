/**
 * Created by blackbookman on 23.03.15.
 */

var assert = require('chai').assert;
var YandexWeather = require('../lib/yandexWeather.js').YandexWeather;
var yandexWeather = new YandexWeather();

var LIPETSK_ID = 27930;

describe('yandexWeather.js', function() {
    it('should get weather data', function(done) {
        yandexWeather.get([LIPETSK_ID]).done(function(result) {
            assert.isArray(result);

            var dataForLipetsk = result[0];

            assert.isObject(dataForLipetsk);
            assert.equal(dataForLipetsk.id, LIPETSK_ID);
            assert.isArray(dataForLipetsk.data);
            assert.lengthOf(dataForLipetsk.data, 8);

            var factWeather = dataForLipetsk.data[0];

            assert.property(factWeather, 'observation_time');
            assert.property(factWeather, 'temperature');
            assert.property(factWeather, 'weather_condition');
            assert.property(factWeather, 'pressure');
            assert.property(factWeather, 'wind_direction');
            assert.property(factWeather, 'wind_speed');
            assert.property(factWeather, 'humidity');

            var tomorrowWeather = dataForLipetsk.data[2];

            assert.property(tomorrowWeather, 'date');
            assert.property(tomorrowWeather, 'day');
            assert.property(tomorrowWeather.day, 'temperature');
            assert.property(tomorrowWeather.day, 'weather_condition');
            assert.property(tomorrowWeather, 'night');
            assert.property(tomorrowWeather.night, 'temperature');
            assert.property(tomorrowWeather.night, 'weather_condition');

            done();
        }, done)
    });
});
