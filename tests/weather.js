/**
 * Created by blackbookman on 24.03.15.
 */
var assert = require('chai').assert;
var YandexWeather = require('../lib/yandexWeather.js').YandexWeather;
var Weather = require('../lib/weather.js').Weather;

var yandexWeather = new YandexWeather();
var weather = new Weather(yandexWeather);

var LIPETSK_ID = 27930;
var SANCT_PETERBURG_ID = 26063;


describe('weather.js', function() {
    it('should save weather data', function(done) {
        yandexWeather
            .get([LIPETSK_ID, SANCT_PETERBURG_ID])
            .then(weather.save)
            .done(function(result) {
                assert.isArray(result);
                assert.lengthOf(result, 2);

                var lipetskResults = result[0];
                var spbResults = result[1];

                assert.isArray(lipetskResults);
                assert.lengthOf(lipetskResults, 3);

                assert.isArray(spbResults);
                assert.lengthOf(spbResults, 3);

                var i = lipetskResults.length;
                while (i--) assert.equal(lipetskResults[i], 'OK');

                var j = spbResults.length;
                while (j--) assert.equal(spbResults[j], 'OK');

                done();
            }, done)
    });

    it('should get weather data for week', function(done) {
        weather.get({daysNum: 7, cityId: LIPETSK_ID}).done(function(dataForLipetsk) {
            assert.isArray(dataForLipetsk);
            assert.lengthOf(dataForLipetsk, 8);

            var factWeather = dataForLipetsk[0];

            assert.property(factWeather, 'observation_time');
            assert.property(factWeather, 'temperature');
            assert.property(factWeather, 'weather_condition');
            assert.property(factWeather, 'pressure');
            assert.property(factWeather, 'wind_direction');
            assert.property(factWeather, 'wind_speed');
            assert.property(factWeather, 'humidity');

            var tomorrowWeather = dataForLipetsk[2];

            assert.property(tomorrowWeather, 'date');
            assert.property(tomorrowWeather, 'day');
            assert.property(tomorrowWeather.day, 'temperature');
            assert.property(tomorrowWeather.day, 'weather_condition');
            assert.property(tomorrowWeather, 'night');
            assert.property(tomorrowWeather.night, 'temperature');
            assert.property(tomorrowWeather.night, 'weather_condition');

            done();
        }, done)
    });

    it('should get weather data for today', function(done) {
        weather.get({daysNum: 1, cityId: LIPETSK_ID}).done(function(dataForLipetsk) {
            assert.isArray(dataForLipetsk);
            assert.lengthOf(dataForLipetsk, 2);

            var factWeather = dataForLipetsk[0];

            assert.property(factWeather, 'observation_time');
            assert.property(factWeather, 'temperature');
            assert.property(factWeather, 'weather_condition');
            assert.property(factWeather, 'pressure');
            assert.property(factWeather, 'wind_direction');
            assert.property(factWeather, 'wind_speed');
            assert.property(factWeather, 'humidity');

            var todayWeather = dataForLipetsk[1];

            assert.property(todayWeather, 'date');
            assert.property(todayWeather, 'day');
            assert.property(todayWeather.day, 'temperature');
            assert.property(todayWeather.day, 'weather_condition');
            assert.property(todayWeather, 'night');
            assert.property(todayWeather.night, 'temperature');
            assert.property(todayWeather.night, 'weather_condition');

            done();
        }, done)
    });

    it('should get weather data for 3 days', function(done) {
        weather.get({daysNum: 3, cityId: LIPETSK_ID}).done(function(dataForLipetsk) {
            assert.isArray(dataForLipetsk);
            assert.lengthOf(dataForLipetsk, 4);

            var factWeather = dataForLipetsk[0];

            assert.property(factWeather, 'observation_time');
            assert.property(factWeather, 'temperature');
            assert.property(factWeather, 'weather_condition');
            assert.property(factWeather, 'pressure');
            assert.property(factWeather, 'wind_direction');
            assert.property(factWeather, 'wind_speed');
            assert.property(factWeather, 'humidity');

            var thirdDayWeather = dataForLipetsk[3];

            assert.property(thirdDayWeather, 'date');
            assert.property(thirdDayWeather, 'day');
            assert.property(thirdDayWeather.day, 'temperature');
            assert.property(thirdDayWeather.day, 'weather_condition');
            assert.property(thirdDayWeather, 'night');
            assert.property(thirdDayWeather.night, 'temperature');
            assert.property(thirdDayWeather.night, 'weather_condition');

            done();
        }, done)
    });
});