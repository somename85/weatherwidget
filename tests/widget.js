/**
 * Created by blackbookman on 24.03.15.
 */

var Widget = require('../lib/widget').Widget;
var assert = require('chai').assert;

var LIPETSK_ID = 27930;
var SANCT_PETERBURG_ID = 26063;

var widget = new Widget({
    possibleCityId: [LIPETSK_ID, SANCT_PETERBURG_ID],
    possibleDaysNum: [1, 3, 7]
});

describe('widget.js', function() {
    it('should read html', function(done) {
        widget
            ._readHTML()
            .done(function(result) {
                assert.isObject(result);
                assert.property(result, 'html');
                assert.typeOf(result.html, 'string');
                done();
            }, done)
    });

    it('should read css', function(done) {
        widget
            ._readCSS()
            .done(function(result) {
                assert.isObject(result);
                assert.property(result, 'css');
                assert.typeOf(result.css, 'string');
                done();
            }, done)
    });

    it('should read js when strategy = source', function(done) {
        widget
            ._readJS({strategy: 'source'})
            .done(function(result) {
                assert.isObject(result);
                assert.property(result, 'js');
                assert.typeOf(result.js, 'string');
                done();
            }, done)
    });

    it('should not read js when strategy != source', function(done) {
        widget
            ._readJS({strategy: 'small'})
            .done(function(result) {
                assert.isObject(result);
                assert.isUndefined(result.js);
                done();
            }, done)
    });

    it('should read js libs when strategy = source', function(done) {
        widget
            ._readJSLibs({strategy: 'source'})
            .done(function(result) {
                assert.isObject(result);
                assert.property(result, 'jsLibs');
                assert.typeOf(result.jsLibs, 'string');
                done();
            }, done)
    });

    it('should not read js libs when strategy != source', function(done) {
        widget
            ._readJSLibs({strategy: 'small'})
            .done(function(result) {
                assert.isObject(result);
                assert.isUndefined(result.jsLibs);
                done();
            }, done)
    });

    it('should create widget', function(done) {
        widget
            .create({cityId: LIPETSK_ID, daysNum: 7})
            .done(function(result) {
                assert.typeOf(result, 'string');
                done();
            }, done)
    });

    it('should create build func when strategy = source', function(done) {
        widget
            ._getBuildFunc({strategy: 'source'})
            .done(function(result) {
                assert.property(result, 'buildFunc');
                assert.typeOf(result.buildFunc, 'string');
                done();
            }, done);
    });

    it('should create build func when strategy != source', function(done) {
        widget
            ._getBuildFunc({strategy: 'small'})
            .done(function(result) {
                assert.isUndefined(result.buildFunc);
                done();
            }, done);
    });

    it('should build options string', function() {
       var result = widget._buildOptionsString(1, 2);
        assert.typeOf(result, 'string');

        var window = {};
        eval(result);

        assert.deepEqual(window.uglyWeatherWidget, { cityId: 1, daysNum: 2 });
    })
});