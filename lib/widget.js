/**
 * Created by blackbookman on 24.03.15.
 */
var fs = require('fs');
var Promise = require('promise');
var util = require('util');
var async = require('async');
var doT = require('dot');

function Widget(obj) {
    var self = this;

    obj = obj || {};

    if (obj.possibleDaysNum && !util.isArray(obj.possibleDaysNum)) throw new Error('possibleDaysNum must be arr');
    if (obj.possibleCityId && !util.isArray(obj.possibleCityId)) throw new Error('possibleCityId must be arr');

    self.possibleDaysNum = obj.possibleDaysNum || [];
    self.possibleCityId = obj.possibleCityId || [];

    self.htmlPath = 'views/widget/min/html.html';
    self.cssPath = 'views/widget/min/css.css';
    self.jsPath = 'views/widget/min/js.js';
    self.jsLibsPath = 'views/widget/lib/';
    self.urlForScript = obj.host + '/script';

    self.template = fs.readFileSync('views/widget/template.html', 'utf8');

    self._getBuildFunc = function(obj) {
        obj = obj || {};
        if (obj.strategy === 'source') obj.buildFunc = 'uglyWeatherWidget.build=' + doT.template(self.template) + ';';
        return Promise.resolve(obj);
    };

    self._readHTML = function(obj) {
        obj = obj || {};
        return new Promise(self._readFile(obj, 'html'));
    };

    self._readCSS = function(obj) {
        obj = obj || {};
        return new Promise(self._readFile(obj, 'css'));
    };

    self._readJS = function(obj) {
        obj = obj || {};
        if (obj.strategy !== 'source') return Promise.resolve(obj);
        return new Promise(self._readFile(obj, 'js'));
    };

    self._readFileHelper = function(obj, property, fulfill, reject) {
        return function(err, result) {
            if (err) reject(err);
            else {
                if (obj[property]) obj[property] += result;
                else obj[property] = result;
                fulfill(obj);
            }
        }
    };

    self._readFile = function(obj, property) {
        return function(fulfill, reject) {
            fs.readFile(
                self[property + 'Path'],
                {encoding: 'utf8'},
                self._readFileHelper(obj, property, fulfill, reject)
            );
        }
    };

    self._readJSLibs = function(obj) {
        obj = obj || {};
        if (obj.strategy !== 'source') return Promise.resolve(obj);
        return new Promise(function(fulfill, reject) {
            fs.readdir(self.jsLibsPath, function(err, fileNames) {
                if (err) reject(err);
                else {
                    if (fileNames.length) {
                        async.each(fileNames, function(filename, callback) {
                            self._readJSLib(obj, filename, callback);
                        }, function(err) {
                            if (err) reject(err);
                            else fulfill(obj);
                        })
                    } else {
                        obj.jsLibs = obj.jsLibs || '';
                        fulfill(obj);
                    }
                }
            });
        })
    };

    self._readJSLib = function(obj, filename, callback) {
        fs.readFile(
            self.jsLibsPath + filename,
            {encoding: 'utf8'},
            function(err, result) {
                if (err) callback(err);
                else {
                    if (obj.jsLibs) obj.jsLibs += result + ';';
                    else obj.jsLibs = result + ';';
                    callback();
                }
            }
        )
    };

    self._buildWidget = function(obj) {
        return new Promise(function(fulfill, reject) {
            if (typeof obj !== 'object') reject(new Error('bad args'));

            switch (obj.strategy) {
                case 'source':
                    obj.html += '<script>';
                    obj.html += obj.options;
                    obj.html += obj.jsLibs;
                    obj.html += obj.buildFunc;
                    obj.html += obj.js;
                    break;
                case 'small':
                    obj.html += '<script src="' + self.urlForScript + '?cityId=' + obj.cityId + '&daysNum=' + obj.daysNum + '">';
                    break;
            }

            obj.html += '</script>';
            obj.html += '<style>' + obj.css + '</style>';

            fulfill(obj.html);
        })
    };

    self._buildJSSource = function(obj) {
        return new Promise(function(fulfill, reject) {
            if (typeof obj !== 'object') reject(new Error('bad args'));

            var result = '';

            result += obj.options;
            result += obj.jsLibs;
            result += obj.buildFunc;
            result += obj.js;

            fulfill(result);
        })
    };

    self._setOptions = function(obj, strategy) {
        return new Promise(function(fulfill, reject) {
            if (!obj || !util.isObject(obj)) reject(new Error('first arg must be obj'));
            if (strategy && strategy !== 'small' && strategy !== 'source') reject(new Error('strategy must be "small" or "source"'));
            if (typeof obj.cityId !== 'number' || typeof obj.daysNum !== 'number') reject(new Error('cityId and daysNum must be numbers'));
            if (!self._isCorrectCityId(obj.cityId)) reject(new Error('unsupported cityId'));
            if (!self._isCorrectDaysNum(obj.daysNum)) reject(new Error('unsupported daysNum'));

            obj.strategy = strategy || 'small';

            if (obj.strategy === 'source') obj.options = self._buildOptionsString(obj.cityId, obj.daysNum);

            fulfill(obj);
        })
    };

    self._buildOptionsString = function(id, days) {
        return 'window.uglyWeatherWidget={cityId:' + id + ',daysNum:' + days + '};';
    };

    self._isCorrectDaysNum = function(num) {
        return ~self.possibleDaysNum.indexOf(num);
    };

    self._isCorrectCityId = function(id) {
        return ~self.possibleCityId.indexOf(id);
    };

    self.create = function(queryObj, strategy) {
        return self._setOptions(queryObj, strategy)
            .then(self._readHTML)
            .then(self._readCSS)
            .then(self._readJSLibs)
            .then(self._getBuildFunc)
            .then(self._readJS)
            .then(self._buildWidget);
    };

    self.getJSSource = function(queryObj) {
        return self._setOptions(queryObj, 'source')
            .then(self._readJSLibs)
            .then(self._getBuildFunc)
            .then(self._readJS)
            .then(self._buildJSSource);
    };
}

exports.Widget = Widget;