/**
 * Created by blackbookman on 23.03.15.
 */

var request = require('request');
var parseString = require('xml2js').parseString;
var Promise = require('promise');
var async = require('async');

function YandexWeather() {
    var self = this;

    self.url = 'http://export.yandex.ru/weather-ng/forecasts/';

    self.get = function(arrayOfCitiesId) {
        return new Promise(function(fulfill, reject) {
            var results = [];

            async.each(arrayOfCitiesId, function(id, callback) {
                self._get(id)
                    .then(self._parseXML)
                    .then(self._removeUnusedFields)
                    .then(function(result) {
                        results.push(result);
                        callback();
                    }, callback);
            }, function(err) {
                if (err) reject(err);
                fulfill(results);
            })
        })
    };

    self._get = function(cityId) {
        return new Promise(function(fulfill, reject) {
            request({uri:self.url + cityId + '.xml'}, function (err, res, xml) {
                if (err || res.statusCode !== 200) reject(err || new Error('' + res.statusCode));
                fulfill(xml);
            });
        })
    };

    self._parseXML = function(xml) {
        return new Promise(function(fulfill, reject) {
            parseString(xml, function (err, result) {
                if (err) reject(err);
                else fulfill(result);
            });
        })
    };

    self._removeUnusedFields = function(obj) {
        //todo: please add city name to saved fields
        return new Promise(function(fulfill, reject) {
            var result = {id: obj.forecast.$.id, data: []};
            var factWeather;

            try {
                factWeather = {
                    observation_time:   obj.forecast.fact[0].observation_time[0],
                    temperature:        obj.forecast.fact[0].temperature[0]._,
                    weather_condition:  obj.forecast.fact[0].weather_condition[0].$.code,
                    pressure:           obj.forecast.fact[0].pressure[0]._,
                    wind_direction:     obj.forecast.fact[0].wind_direction[0],
                    wind_speed:         obj.forecast.fact[0].wind_speed[0],
                    humidity:           obj.forecast.fact[0].humidity[0]
                };
            } catch (err) {
                reject(err);
            }


            result.data.push(factWeather);

            try {
                var daysWeather = obj.forecast.day;
                var tmp;

                for (var i = 0; i < 7; i++) {
                    tmp = {
                        date: daysWeather[i].$.date,
                        day: {
                            temperature: daysWeather[i].day_part[4].temperature[0],
                            weather_condition: daysWeather[i].day_part[4].weather_condition[0].$.code
                        },
                        night: {
                            temperature: daysWeather[i].day_part[5].temperature[0],
                            weather_condition: daysWeather[i].day_part[5].weather_condition[0].$.code
                        }
                    };

                    result.data.push(tmp);
                }
            } catch (err) {
                reject(err);
            }

            fulfill(result);
        })
    }
}

exports.YandexWeather = YandexWeather;