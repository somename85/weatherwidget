/**
 * Created by blackbookman on 24.03.15.
 */

var redis = require("redis");
var Promise = require('promise');
var async = require('async');
var config = require('../config');

function Weather(weatherProvider) {
    if (!weatherProvider) throw new Error('weatherProvider must be define');

    var self = this;

    self.db = redis.createClient(config.redis.port, config.redis.host, config.redis.options);
    self.weatherProvider = weatherProvider;

    //todo: remove method!!!
    self.updateWeather = function(idsArr, delay) {
        //better use cron, please
        if (delay) setTimeout(function() {
            self.updateWeather(idsArr, delay);
        }, delay);

        return self.weatherProvider
            .get(idsArr)
            .then(self.save);
    };

    self.save = function(datas) {
        return new Promise(function(fulfill, reject) {
            var results = [];

            async.each(datas, function(data, callback) {
                self._stringifyData(data).then(self._save).then(function(result) {
                    results.push(result);
                    callback();
                }, callback);
            }, function(err) {
                if (err) reject(err);
                else fulfill(results);
            })
        })
    };

    self.get = function(queryObj) {
        return self._get(queryObj).then(self._parseData);
    };

    self._save = function(data) {
        return Promise
            .all([
                self._saveWeatherForOneDay(data),
                self._saveWeatherThreeDays(data),
                self._saveWeatherForWeek(data)
            ])
    };

    self._saveWeatherForOneDay = function(data) {
        return new Promise(function(fulfill, reject) {
            self.db.set(self._createKey(data.id, 1), data.oneDay, function(err, result) {
                if (err) reject(err);
                else fulfill(result);
            });
        })
    };

    self._saveWeatherThreeDays = function(data) {
        return new Promise(function(fulfill, reject) {
            self.db.set(self._createKey(data.id, 3), data.threDays, function(err, result) {
                if (err) reject(err);
                else fulfill(result);
            });
        })
    };

    self._saveWeatherForWeek = function(data) {
        return new Promise(function(fulfill, reject) {
            self.db.set(self._createKey(data.id, 7), data.week, function(err, result) {
                if (err) reject(err);
                else fulfill(result);
            });
        })
    };

    self._createKey = function(stringId, stringDayNum) {
        return stringId + '::' + stringDayNum;
    };

    self._get = function(queryObj) {
        return new Promise(function(fulfill, reject) {
            self.db.get(self._createKey(queryObj.cityId, queryObj.daysNum), function(err, result) {
                if (err) reject(err);
                else fulfill(result);
            });
        })
    };

    self._stringifyData = function(data) {
        var result = {id: data.id};

        return new Promise(function(fulfill, reject) {
            try {
                result.oneDay = JSON.stringify(data.data.slice(0, 2));
                result.threDays = JSON.stringify(data.data.slice(0, 4));
                result.week = JSON.stringify(data.data);
            } catch (err) {
                reject(err);
            }

            fulfill(result);
        })
    };

    self._parseData = function(data) {
        return new Promise(function(fulfill, reject) {
            try {
                fulfill(JSON.parse(data));
            } catch (err) {
                reject(err);
            }
        })
    };

    self.db.on('error', function (err) {
        console.log(err.stack);
        //todo: handle err
    });
}

exports.Weather = Weather;