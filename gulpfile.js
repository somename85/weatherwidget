var gulp = require('gulp'),
    minifyCSS = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    minifyHTML = require('gulp-minify-html');

var dist = __dirname + '/views/widget/min';

gulp.task('minify-widget-html', function() {
    return gulp.src(__dirname + '/views/widget/html.html')
        .pipe(minifyHTML())
        .pipe(gulp.dest(dist));
});

gulp.task('minify-widget-css', function() {
    return gulp.src(__dirname + '/views/widget/*.css')
        .pipe(minifyCSS({keepBreaks:true}))
        .pipe(gulp.dest(dist));
});

gulp.task('minify-widget-js', function() {
    gulp.src(__dirname + '/views/widget/*.js')
        .pipe(uglify())
        .pipe(gulp.dest(dist));
});

gulp.task('build_widget', ['minify-widget-html', 'minify-widget-css', 'minify-widget-js']);