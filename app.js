/**
 * Created by blackbookman on 23.03.15.
 */

var express = require('express');
var fs = require('fs');

var YandexWeather = require('./lib/yandexWeather.js').YandexWeather;
var Weather = require('./lib/weather.js').Weather;
var Widget = require('./lib/widget.js').Widget;

var yandexWeather = new YandexWeather();
var weather = new Weather(yandexWeather);
var app = express();

var config = require('./config');
var adminPanel = fs.readFileSync(__dirname + '/views/panel.html');

var widget = new Widget({
    possibleCityId: config.citiesIds,
    possibleDaysNum: [1, 3, 7],
    host: config.href
});

weather.updateWeather(config.citiesIds, 1000 * 60 * 30);

app
    .use(express.static(__dirname + '/public'))
    .get('/', function (req, res) {
        res.set('Content-Type', 'text/html');
        res.send(adminPanel);
    })
    .get('/weather', function (req, res) {
        var daysNum = +req.query.daysNum;
        var cityId = +req.query.cityId;

        if (typeof daysNum === 'number' && typeof cityId === 'number')  {
            weather
                .get({daysNum: daysNum, cityId: cityId})
                .done(function(result) {
                    res.jsonp(result);
                }, function(err) {
                    res.status(500).send(err.message);
                })
        } else {
            res.status(400).send('Wrong query');
        }
    })
    .get('/widget', function(req, res) {
        var daysNum = +req.query.daysNum;
        var cityId = +req.query.cityId;


        if (typeof daysNum === 'number' && typeof cityId === 'number')  {
            widget
                .create({daysNum: daysNum, cityId: cityId}, req.query.strategy)
                .done(function(result) {
                    res.send(result);
                }, function(err) {
                    res.status(500).send(err.message);
                });
        } else {
            res.status(400).send('Wrong query');
        }
    })
    .get('/script', function(req, res) {
        var daysNum = +req.query.daysNum;
        var cityId = +req.query.cityId;

        if (typeof daysNum === 'number' && typeof cityId === 'number')  {
            widget
                .getJSSource({daysNum: daysNum, cityId: cityId})
                .done(function(result) {
                    res.set('Content-Type', 'application/javascript');
                    res.send(result);
                }, function(err) {
                    res.status(500).send(err.message);
                });
        } else {
            res.status(400).send('Wrong query');
        }
    })
    .listen(config.port, function () {
        console.log('Ugly weather widget server was started on ' + config.port + ' port.')
    });