/**
 * Created by blackbookman on 28.03.15.
 */
var config = require('./config');
var YandexWeather = require('./lib/yandexWeather.js').YandexWeather;
var Weather = require('./lib/weather.js').Weather;

var yandexWeather = new YandexWeather();
var weather = new Weather(yandexWeather);

weather.updateWeather(config.citiesIds);